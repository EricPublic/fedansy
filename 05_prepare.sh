#!/bin/sh
# prepare the inventory for usage of Fedansy
# Usage:
#	git clone https://gitlab.com/EricPublic/fedansy.git && \
#	cd fedansy && \
#       ./05_prepare.sh

INVENTORY_DIR=inventory.myown.d
TARGET_HOSTS=${INVENTORY_DIR}/hosts

mkdir -pv ${INVENTORY_DIR}/group_vars/{new_pcs,old_pcs,web_servers}
cp -nv samples/hosts.sample ${TARGET_HOSTS}

for role in $(ls -1d roles/*_*)
do
	role_name=$(basename ${role})
	cp -nv ${role}/defaults/main.yml \
		${INVENTORY_DIR}/group_vars/new_pcs/${role_name}.yml
done

echo "==> Don't forget to edit '${TARGET_HOSTS}' according to comments <==" >&2
echo "Also don't forget to edit all above copied YAML files" >&2



#!/bin/sh
# Script to install the pre-requisites required by Fedansy
# Make sure you have sudo rights, or call directly as root
sudo dnf install \
	ansible \
	git \
	pykickstart \
	curl \
	mediawriter

#!/bin/sh
# script to call locally in order to avoid having ansible fail on upgrade of itself
sudo dnf -y upgrade
sudo -v  # just to be sure the credentials are still available after the upgrade
ansible-playbook ${0%.sh}.yml --skip-tags=upgrade_all_packages "$@"
